export const environment = {
  production: false,
  apiUrl: 'https://api.punkapi.com/v2',
  requestTimeout: 30000
};
