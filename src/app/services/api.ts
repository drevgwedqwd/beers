import { Headers, Http, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Response } from '@angular/http';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class ApiService {
  constructor(
    private http: Http,
  ) { }

  public async get(endpoint: string, params: any = {}, options?: RequestOptions): Promise<any> {
    const requestOptions = await this.prepareRequestOptions(options);

    return this.http
      .get(`${environment.apiUrl}${endpoint}/`, Object.assign(requestOptions, { params: params }))
      .timeout(environment.requestTimeout)
      .toPromise()
      .then((response) => this.prepareResponse(response))
      .catch((errorResponse) => console.error(errorResponse));
  }

  private prepareBody(data: any): string {
    return JSON.stringify(data);
  }

  private async prepareRequestOptions(options?: RequestOptions): Promise<RequestOptions> {
    let requestOptions = new RequestOptions({
      headers: await this.generateDefaultHeaders()
    });

    if (options) {
      requestOptions = Object.assign(requestOptions, options);
    }

    return requestOptions;
  }

  private async generateDefaultHeaders(): Promise<Headers> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return headers;
  }

  private prepareResponse(response: Response): any {
    return response.json();
  }
}