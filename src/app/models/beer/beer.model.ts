import { IBeer } from './beer.interface';

export class Beer implements IBeer {
    public id: number;
    public name: string;
    public tagline: string;
    public first_brewed: string;
    public description: string;
    public image_url: string;

    constructor(beer: any = {}) {
        this.id = beer.id || null;
        this.name = beer.name || null;
        this.tagline = beer.tagline || null;
        this.first_brewed = beer.first_brewed || null;
        this.description = beer.description || null;
        this.image_url = beer.image_url || null;
    }
}