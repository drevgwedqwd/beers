import { ApiService } from './services/api';
import { AppComponent } from './app.component';
import { BeerCardComponent } from './components/beerCard/beerCard.component';
import { BeerService } from './services/beer';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [
    AppComponent,
    BeerCardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgbModule.forRoot()
  ],
  providers: [
    ApiService,
    BeerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
